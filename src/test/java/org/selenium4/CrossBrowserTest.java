package org.selenium4;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.net.MalformedURLException;
import java.net.URL;

public class CrossBrowserTest {
    /*  protected static ChromeDriver driver; */
    WebDriver driver = null;
    String URL = "https://simple.ripley.com.pe/huawei-router-wi-fi-ws318n-2004262505264p";
    public static String status = "passed";
    String username = "perftestavatar007";
    String access_key = "h3XVEFIHhZWu7qeXcGpq82RrOvZWOtTsvocxRu1lFqb6P5HVxX";

    @BeforeClass
    public void testSetUp() throws MalformedURLException {
        /*
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        */
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("build", "Maven Parallel Testing with Jenkins Pipeline [Chrome]");
        capabilities.setCapability("name", "Maven Parallel Testing with Jenkins Pipeline [Chrome]");
        capabilities.setCapability("platform", "Windows 10");
        capabilities.setCapability("browserName", "Chrome");
        capabilities.setCapability("version","latest");
        capabilities.setCapability("tunnel",false);
        capabilities.setCapability("network",true);
        capabilities.setCapability("console",true);
        capabilities.setCapability("visual",true);

        driver = new RemoteWebDriver(new URL("http://" + username + ":" + access_key + "@hub.lambdatest.com/wd/hub"), capabilities);
        System.out.println("Started session");
    }

    @Test
    public void test_Selenium4_ToDoApp() throws InterruptedException {
        driver.navigate().to(URL);
        driver.manage().window().maximize();

        try {
            Thread.sleep(20000);
            driver.findElement(By.cssSelector("button#onesignal-slidedown-cancel-button.align-right.secondary.slidedown-button")).click();
            Thread.sleep(7000);
            driver.findElement(By.cssSelector("button#buy-button.btn-loading.btn-primary.btn-loading.js-buy-button.with-withlist")).click();
            //Thread.sleep(7000);
            //driver.findElement(By.cssSelector("div.button > button")).click();
            Thread.sleep(4000);
            driver.findElement(By.id("stepper-button")).click();
            Thread.sleep(7000);
            //driver.findElement(By.cssSelector("button#guestShopperContinue.btn.btn-primary")).click();
            driver.findElement(By.id("btn-continue")).click();
            Thread.sleep(7000);
            //driver.findElement(By.id("WC_CheckoutLogon_FormInput_logonId")).click();
            //driver.findElement(By.id("WC_CheckoutLogon_FormInput_logonId")).clear();
            //driver.findElement(By.id("WC_CheckoutLogon_FormInput_logonId")).sendKeys("75268234");
            //driver.findElement(By.id("WC_CheckoutLogon_FormInput_logonPassword")).clear();
            //driver.findElement(By.id("WC_CheckoutLogon_FormInput_logonPassword")).sendKeys("tremendaloka01");
            driver.findElement(By.name("ws_username")).click();
            driver.findElement(By.name("ws_username")).clear();
            driver.findElement(By.name("ws_username")).sendKeys("75268234");
            driver.findElement(By.name("password")).clear();
            driver.findElement(By.name("password")).sendKeys("tremendaloka01");
            //driver.findElement(By.cssSelector("button.btn.btn-secondary.center")).click();
            driver.findElement(By.cssSelector("button.btn-loading.btn-ripley")).click();
            Thread.sleep(15000);
            driver.findElement(By.id("singleShipmentPhysicalStore_0_17017")).click();
            Thread.sleep(10000);
            driver.findElement(By.id("WC_ShipmentDisplay_continue")).click();
            Thread.sleep(10000);
            driver.findElement(By.id("WC_ShipmentDisplay_continue_modal")).click();
            Thread.sleep(10000);
            driver.findElement(By.cssSelector("span#payMethodId_1_3_etiqueta_icono.name")).click();
            //Thread.sleep(10000);
            //driver.findElement(By.id("shippingBillingPageNext")).click();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @AfterClass
    public void tearDown() {
        if (driver != null) {
            ((JavascriptExecutor) driver).executeScript("lambda-status=" + status);
            driver.quit();
        }
    }
}